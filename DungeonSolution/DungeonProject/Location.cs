﻿using System;

namespace DungeonProject
{
	public class Location
	{

		string m_description;
		string m_name;

		Location m_connection_North;
		Location m_connection_South;
		Location m_connection_West;
		Location m_connection_East;

		public Location (string name, string description)
		{
			m_name = name;
			m_description = description;
		}

		public void PrintConnection(string key, Location nextConnection)
		{
			if (nextConnection != null) 
			{
				Console.WriteLine ("Drücke "  + key +  " um nach "  + nextConnection.m_name +  " gehen");
			}
		}

		public void CreateConnections(Location north, Location south, Location west, Location east)
		{
			m_connection_North = north;
			m_connection_South = south;
			m_connection_West = west;
			m_connection_East = east;
		}
		/// <summary>
		/// Prints the description.
		/// </summary>
		public void PrintDescription()
		{
			Console.Clear ();
			Console.WriteLine ("Du bist jetzt in " +  m_name  + "\n");
			Console.WriteLine (m_description + " \n");

			PrintConnection ("hoch", m_connection_North);
			PrintConnection ("runter", m_connection_South);
			PrintConnection ("links", m_connection_West);
			PrintConnection ("rechts", m_connection_East);
			/// beschreibung der tasten
			ConsoleKey key = Console.ReadKey ().Key;
			switch (key)
			{
			case(ConsoleKey.UpArrow):
				{
					if (m_connection_North != null) {m_connection_North.PrintDescription ();}
					break;
				}
			case(ConsoleKey.DownArrow):
				{
					if (m_connection_South != null) {m_connection_South.PrintDescription ();}
					break;
				}
			case(ConsoleKey.LeftArrow):
				{
					if (m_connection_West != null) {m_connection_West.PrintDescription ();}
					break;
				}
			case(ConsoleKey.RightArrow):
				{
					if (m_connection_East != null) {m_connection_East.PrintDescription ();}
					break;
				} /// bestimmt das bei null keine verbindung besteht und ansonsten beschreibungen der räume gegeben werden
			}
			PrintDescription ();
		}

	}
}

