﻿using System;

namespace DungeonProject
{
	public class MainGame
	{
		

		public static void Raum ()
		{
			
			Location rezeption = new Location ("der Großen Halle ", "Eine riesige im Kolonialstil eingerichtete Halle in deren Mitte sich die Rezeption befindet.");
			Location flur = new Location ("dem großen Flur", "Ein langer gekachelter Flur");
			Location gruppenschlafraum = new Location ("dem Gruppenschlafraum", "5 Doppelstockbetten mit dünnen durchgelegenen Matrazen, ein leichter Aminiak Geruch liegt in der Luft");
			Location patientenraum = new Location ("deinem eigenem Zimmer", "Ein Bett, eine Toilette und ein vergittertes Fenster durch das du gerade so auf Zehenspitzen durchschauen kannst");
			Location gang = new Location ("dem Gang welcher duch zum Therapiezimmer führt", "eklig mit seltsam schmierigen boden");
			Location therapiezimmer = new Location ("dem Therapiezimmer von Dr. Namefolgt", "der mit Abstand am besten eingerichtete Raum. Bequeme Sofas ein hölzener Tisch und Bücherregale gefüllt mit PsychologieLektüre");
			Location gemeinschaftszimmer = new Location ("einem großer weitläufiger Raum", "wahrscheinlich das einzige Zimmer was Fenster besitzt durch die man ohne große bemühungen sehen kann");
			Location waschraum = new Location ("dem Waschsaal", "wenn man sich den Rest der Anstalt ansieht wundert es einen nicht das hier ebenfalls kaum hygienische zustände herrschen");
			///beschreibungen für die räume
			rezeption.CreateConnections (null, flur, null, null);
			flur.CreateConnections (rezeption, gang, gruppenschlafraum, patientenraum);
			gruppenschlafraum.CreateConnections (null, gemeinschaftszimmer, null, flur);
			patientenraum.CreateConnections (gang, null, null, null);
			gang.CreateConnections (flur, patientenraum, gemeinschaftszimmer, therapiezimmer);
			therapiezimmer.CreateConnections (null, null, gang, null);
			gemeinschaftszimmer.CreateConnections (gruppenschlafraum, waschraum, null, gang);
			waschraum.CreateConnections (gemeinschaftszimmer, null, null, null);
			///verbindungen der räume


		}

	}
	
}



